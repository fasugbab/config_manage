#!/bin/bash

######################################################
#
# this is the script that should be ran after 
# linuxjobber application has been deployed
# it will help set up all the little environments 
# setup not covered by ansible
#
######################################################

# For every file that needs to be owned by linuxjobber user, set permission
sudo chown -R linuxjobber:linuxjobber /tools/
sudo chown -R linuxjobber:linuxjobber /oldtools

# untar old tools
cd /oldtools && sudo chown linuxjobber:linuxjobber ./* && tar xvf ./*.tar

# ensure backup is running and aws is connected
sudo pip3 install awscli #dont put the --user because awscli will not be available to sudo
mkdir -p /home/linuxjobber/.local/bin/
ln -s /usr/local/bin/aws /home/linuxjobber/.local/bin/aws
sudo cp -r /home/linuxjobber/.aws /root
sudo chown linuxjobber:linuxjobber /mnt/backup_linux.sh
sudo chmod u+x /mnt/backup_linux.sh
