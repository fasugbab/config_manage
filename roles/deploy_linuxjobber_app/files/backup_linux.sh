#!/usr/bin/bash

#setting up on a new machine, aws cli version1
#first you have to install aws globally with the following command:
#sudo pip3 install awscli #dont put the --user because awscli will not be available to sudo
#create the directory where local user shell expects aws binaries to reside
#mkdir -p /home/linuxjobber/.local/bin/
#next link the aws global installation with the linuxjobbers homedir:
#ln -s /usr/local/bin/aws /home/linuxjobber/.local/bin/aws
#after the above, copy the aws s3 credentials in linuxjobber home to root and youll be able to run this script
#sudo cp -r /home/linuxjobber/.aws /root
#ensure user can access and run file
#sudo chown linuxjobber:linuxjobber /mnt/backup_linux.sh
#sudo chmod u+x /mnt/backup_linux.sh

HOST='ljdb'
PORT='3306'
USER='root'
PASSWORD='fly2Lag0s'
DATABASE='linuxjobber2'
BACKUP_PATH='/mnt/backups'
DATE=`date +%d-%m-%Y`
DATETIME=`date +%d-%m-%Y_%H-%M-%S`
BACKUP_NAME='ljdb'
EXT='sql'

sudo chown -R linuxjobber:linuxjobber /mnt/ /opt/scripts/
sudo chmod u+x /mnt/backup_linux.sh /opt/scripts/s3_sync_media_files.sh

if [[ ! -e $BACKUP_PATH ]]; then
    mkdir $BACKUP_PATH
fi

if [[ -d $BACKUP_PATH/all-${DATE} ]]; then
    DATE=$DATETIME
fi

docker exec lj_db sh -c 'exec mysqldump --routines --all-databases -uroot -pfly2Lag0s' >${BACKUP_PATH}/all-${DATE}.${EXT}
/home/linuxjobber/.local/bin/aws s3 sync /mnt/backups s3://assets.prod.linuxjobber.com/backups_db/
if [ $? -eq 0 ]; then
        echo "Database backup succesfully" && find /mnt/backups -name '*.sql' -type f -mtime +7 -delete
	/home/linuxjobber/.local/bin/aws s3 sync /mnt/backups s3://assets.prod.linuxjobber.com/backups_db/ --delete
else
        echo "Error while backing up" && exit 3
fi

