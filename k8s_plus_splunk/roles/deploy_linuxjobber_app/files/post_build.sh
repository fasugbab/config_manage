#!/bin/bash

######################################################
#
# this is the script that should be ran after 
# linuxjobber application has been deployed
# it will help set up all the little environments 
# setup not covered by ansible
#
######################################################

# untar old tools
cd /oldtools && sudo chown linuxjobber:linuxjobber ./* && tar xvf ./*.tar
