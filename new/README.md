*To deploy stage*

  STEP 1

	- Change the following hosts in the hosts file

		-[deploy_linuxjobber_app]

		-[deploy_chatscrum_app]
		
		-[pull_run_mysql]
		
		-[pull_run_redis]
		
		-[load_db_from_s3]
		
		-[deploy_splunk]

  *STEP 2*
  
  	- Run the script this way
  	
  		ansible-playbook --ask-vault-pass deploy_linuxjobber_stage.yml

  		input the vault pass.

*To deploy the splunk forwarder on other machine where the logs are going to be forwarded from*

  STEP 1

  	- Change the host below in the hosts file

  		- [deploy_splunk_forwarder]

  STEP 2
  
  	- Run the script this way

  		ansible-playbook --ask-vault-pass deploy_splunk_forwarder.yml		 

  		input the vault pass. 		